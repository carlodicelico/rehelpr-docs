Welcome to ReHelpr!
===================

---

>"As you grow older, you will discover that you have two hands, one for helping yourself, the other for helping others."
>**Audrey Hepburn**

People help people.
-------------------

People want to help each other. People helping each other is at the heart of what community is all about. Just think about all of the people on Wikipedia, Craigslist, and Reddit, the millions of people who volunteer each year for various causes - the breast cancer walkers and blood donors, the buddies who help you move and the strangers who give you directions. That's a lot of people helping people!

Sometimes, though, helping someone isn't as much fun as we'd like it to be. On the flip side of that interaction, sometimes it isn't as easy to get the help we need as it should be. What if we could bring more *fun* to helping people and make getting help *easy*? ReHelpr is a social game that tries to do just that.

How does it work (for users)?
-----------------------------

The first step is to sign up for an account. ReHelpr works on the web and on your mobile device, so you can sign up either way. You can sign up with your existing social networking account or create a separate ReHelpr account. 

##### ReHelp Me Bucks (H$)

You can think of ReHelp Me Bucks as being just like gold coins in games like WarCraft. When you help someone, she gives you some ReHelp Me Bucks. You two figure out how much you should get and she sends the bucks to you online or by bumping your phones together. Then, you can pass the bucks on to whoever helps *you*!

##### Points (Pts)

Points are...well, points are points. You get points for playing the game, whether you're helping someone or being helped. You get *more* points for helping, though. Points go toward earning badges and determine what quests are available for you to play. 

##### Badges

As you play the game, you'll earn badges. They'll just pop up when you get one. Other than that, they won't get in your way. Badges are basically like bragging rights. They show up on your profile for all your friends to see.

##### Quests

Quests are mini games. Different quests will become available as you play.

##### Levels

Levels are used internally by ReHelpr to rank players but aren't in the user interface anywhere. They just let us know who the top players are. When you get to the higher levels, you'll become eligible to help create badges, quests, and rewards.

##### Rewards

Rewards are random prizes given out by ReHelpr without notice at any given time, at our discretion. They could be tied to a quest or not, to a badge, or not, or maybe just given to someone for doing something extraordinary.

##### Achievements

TBD

##### Reputation

TBD

##### Social

ReHelpr can integrate with Facebook, Twitter, and Google, so you can share all of your activity automatically across any or all of your social networking accounts.

How does it work? (for biz people)
----------------------------------

##### Mission Statement

ReHelpr is a social game that helps people help people.

##### What's the value of a ReHelp Me Buck?

The 'market' - defined as the total number of players collectively at any given time - decides the value of ReHelp Me Bucks for that locale.

##### What's the value of a point?

The 'system' keeps the value of points at a fixed rate that is expressed as a valuation of the player's activity and the agreed-upon value of that single action (in $H) between parties.

##### That's really sweet. How will it make money?

Ads? Sales of $H? Accessories?

How does it work? (for geeks)
-----------------------------

##### Web App

##### Mobile App

##### Data

#### APIs

ReHelpr uses the following APIs: OAuth, Facebook Connect, Twitter, Google, OpenGraph, Bump, and Parse.

The ReHelpr API for developers exposes our core game engine for social web & mobile games.

##### All Together Now!

![ReHelpr's High-level Architecture](https://gitlab.com/carlodicelico/rehelpr-docs/raw/master/img/hla_diagram.png "ReHelpr's High-level Architecture")

FAQ
---

